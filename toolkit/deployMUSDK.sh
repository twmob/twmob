#!/bin/bash -e
#set -e
PATH="$PATH:$HOME/.rvm/bin"
source $(rvm env --path -- 2)

git fetch
git checkout -f master
git reset --hard origin/master

sh fetch.sh --prefix "../MUSDK" MUSDK
sh fetch.sh --prefix "../MUSDK" MUSDKSpec

git add ../MUSDK/*
git commit -a -m"Upload MUSDK"
git push origin master

pod repo push twmob ../MUSDK/*.podspec
