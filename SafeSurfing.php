<?php
/*
 * Setup default time zone
 */
date_default_timezone_set('Asia/Taipei');

$title = 'SafeSurfing for Shellshock';
$host = 'https://twmob.azurewebsites.net/';
$plist = 'https://twmob.azurewebsites.net/install_plist.php';
$builds = array(array('dir' => 'SafeSurfing/', 'bundle' => 'com.TrendMicro.SmartSurfing', 'type' => 'BETA'));
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Install SafeSurfing 2.6</title>
  <link rel=stylesheet type="text/css" href="styles/style.css">
</head>
<body>
<?php
foreach($builds as $build) {
?>
<a href="#<?=$build['type']?>"><?=$build['type']?></a>
<?php } ?>

<br/>

<?php
foreach($builds as $build) {
?>

<a name="<?=$build['type']?>"><h2><?=$build['type']?></h2></a><a href="#Top">Top</a>
<?php
  $files = array();
  if ($handle = opendir($build['dir'])) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != ".." && pathinfo($file, PATHINFO_EXTENSION) == "ipa") {
        $files[]=$file;
      }
    }
    closedir($handle);
    arsort($files);
  }

  foreach($files as $file) {
    $date = date("Y/m/d H:i:s.", filemtime($build['dir'] . $file));
    $version = explode("_", $file)[3];
    $data = array('url'   => $host.$build['dir'].$file,
                  'title' => $title,
                  'version' => $version,
                  'bundle' => $build['bundle']);
    $url = urlencode($plist.'?'.http_build_query($data));
?>
<div class="step">
  <table><tr>
    <td class="instructions">Install the<br />SafeSurfing 2.6 app<br />Build: <?=$version?><br/>Release Date: <?=$date?></td>
    <td width="24" class="arrow">&rarr;</td>
    <td width="57" class="imagelink">
      <a href="itms-services://?action=download-manifest&url=<?=$url?>">
        <img src="images/SafeSurfing.png" height="57" width="57" />
      </a>
    </td>
  </tr></table>
</div>
<?php
  }
}
?>
</body>
</html>
