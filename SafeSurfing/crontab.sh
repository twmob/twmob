#!/bin/bash -e

## Get global builds

git fetch
git checkout -f master
git reset --hard origin/master

sh fetch.sh --prefix "./" SS

git add ./*
git commit -a -m"Upload SafeSurfing"
git push origin master