#!/bin/bash -e

## Get global builds
sh fetch.sh --prefix "en_beta" iTMMS
sh fetch.sh --prefix "ja_beta" --lang "ja" iTMMS
