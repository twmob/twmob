#!/bin/bash -e
set -e

## fetch
function fetch() { #$1=srcPath, $2=dstPath
  srcPath="$1"; dstPath="$2"
  srcName=$(basename "$srcPath")
  dstName=$(basename "$dstPath")
  if cmp -s "$srcPath" "$dstPath"; then
    echo "$dstName is already up-to-date ($srcName)"
  else
    dir="$(dirname $dstPath)"
    mkdir -p "$dir"
    cp -f "$srcPath" "$dir"
    chmod +r "$dir/$srcName"
    mv -f "$dir/$srcName" "$dir/$dstName"
    echo "$srcPath copied"
  fi
}

## find iTMMS build
function findiTMMS() { #$1=version, $2=lang, $3=build, $4=filter
  version=$1; lang=$2; build=$3; filter=$4
  srcPath="$mountPoint/iTMMS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath/$filter" ] || `echo "Can't find the build in $srcPath/$filter" 1>&2; return 1`
  result=$(find "$srcPath/$filter" -name "*.ipa" | tail -n1)
  [ -e "$result" ] || `echo "Can't find the build in $srcPath/$filter" 1>&2; return 1`
  build=$(expr "$(basename $result)" : '.*\([0-9][0-9][0-9][0-9]\).*')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find MUAgent build
function findMUA() { #$1=version, $2=lang, $3=build, $4=filter
  version=$1; lang=$2; build=$3; filter=$4
  srcPath="$mountPoint/MUP_iOS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath/$filter" ] || `echo "Can't find the build" 1>&2; return 1`
  result=$(find "$srcPath/$filter" -name "*.ipa" | tail -n1)
  build=$(expr "$(basename $result)" : '.*\([0-9][0-9][0-9][0-9]\).*')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find MUSDK build
function findMUSDK() { #$1=version, $2=lang, $3=build, $4=filter
  version=$1; lang=$2; build=$3; filter=$4
  srcPath="$mountPoint/MUP_iOS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath" ] || `echo "Can't find the build" 1>&2; return 1`
  result=$(find "$srcPath" -name "*$fileter*.zip" | tail -n1)
  [ -e "$result" ] || `echo "Can't find the build in $srcPath" 1>&2; return 1`
  build=$(expr "$(basename $result)" : '.*\([0-9][0-9][0-9][0-9]\).*')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find MUSDK podspec
function findMUSDKSpec() { #$1=version, $2=lang, $3=build
  version=$1; lang=$2; build=$3
  srcPath="$mountPoint/MUP_iOS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath" ] || `echo "Can't find the build" 1>&2; return 1`
  result=$(find "$srcPath" -name "*.podspec" | tail -n1)
  [ -e "$result" ] || `echo "Can't find the build in $srcPath" 1>&2; return 1`
  build=$(grep "s.version" MUSDK.podspec | sed 's/.*\"\(.*\)\"/\1/')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find JB build
function findJB() { #$1=version, $2=lang, $3=build, $4=filter
  version=$1; lang=$2; build=$3; filter=$4
  srcPath="$mountPoint/JewelryBox_Client_iOS/$version/mac10_8/$lang/Rel/$build/Release/Output/Distribution-iphoneos"
  [ -d "$srcPath" ] || `echo "Can't find the build" 1>&2; return 1`
  result=$(find "$srcPath" -name "*$filter.ipa" | tail -n1)
  filename=$(printf '%q' $(basename "$result"))
  build=$(expr \"$filename\" : '.*\([0-9][0-9][0-9][0-9]\).*')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find DP build
function findDP() { #$1=version, $2=lang, $3=build, $4=filter
  version=$1; lang=$2; build=$3; filter=$4
  srcPath="$mountPoint/DirectPass_iOS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath" ] || `echo "Can't find the build" 1>&2; return 1`
  result=$(find "$srcPath" -name "*$filter.ipa" | tail -n1)
  build=$(expr "$(basename $result)" : '.*\([0-9][0-9][0-9][0-9]\).*')
  echo "$version $lang $build $filter"
  echo "$result"
}

## find SmartSurfing build
function findSS() { #$1=version, $2=lang, $3=build, $4=filter
  local version=$1; local lang=$2; local build=$3; local filter=$4
  local srcPath="$mountPoint/SmartSurfing4iOS/$version/mac10_8/$lang/Rel/$build/Release/Output"
  [ -d "$srcPath" ] || `echo "Can't find the build" 1>&2; return 1`
  local result=$(find "$srcPath" -name "*$filter.ipa" | tail -n1)
  local logPath="$mountPoint/SmartSurfing4iOS/$version/mac10_8/$lang/Rel/$build/Release/Build_Log"
  local build=$(basename $(find "$logPath" -name "*.txt") | cut -d'.' -f1)
  echo "$version $lang $build $filter"
  echo "$result"
}

## Usage
function usage() {
  echo
  echo "Fetch ETS build - command line interface."
  echo
  echo "fetch [Options] <ProjectName>"
  echo
  echo "Options:"
  echo "-h|--help    Show this info."
  echo "-v|--version Set Version"
  echo "-l|--lang    Set Language"
  echo "-b|--build   Set Build (default is Latest)"
  echo "-f|--filter  Set search filter for build"
  echo "-p|--prefix  Set prefix of destination (default is build)"
  echo "-t|--target  Set target name"
  echo
}

## Parse the options
prefix="build"
while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help)
      usage
      exit 0
      ;;
    -v|--version)
      version="$2"
      shift
      ;;
    -l|--lang)
      lang="$2"
      shift
      ;;
    -b|--build)
      build="$2"
      shift
      ;;
    -f|--filter)
      filter="$2"
      shift
      ;;
    -p|--prefix)
      prefix="$2"
      shift
      ;;
    -t|--target)
      target="$2"
      shift
      ;;
    *) ;;
  esac

shift
done

## Setup project name
projectName="$1"

if [ -z "$*" ];then
    usage
    exit 1
else

    ## Mount Samba file server and copy latest build
    mountPoint="/Volumes/Build-1"
    if [ ! -d "$mountPoint" ];then
        echo "Mount File server to $mountPoint..."
        read -p "User account:" userName
        mkdir -p "$mountPoint"
        mount_smbfs "//$userName@tw-ets-fs/Build" "$mountPoint" || \
            rm -rf "$mountPoint"
    fi

    case $projectName in
      iTMMS)
        [ -z $version ] && version="2.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="Demo_beta"
        result=$(findiTMMS $version $lang $build $filter)
        ;;
      MUA)
        [ -z $version ] && version="1.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="Demo_beta"
        result=$(findMUA $version $lang $build $filter)
        ;;
      MUSDK)
        [ -z $version ] && version="1.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter=""
        result=$(findMUSDK $version $lang $build $filter)
        [ -z $target ] && target="$(basename $(echo "$result" | tail -n1))"
        ;;
      MUSDKSpec)
        [ -z $version ] && version="1.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="rel"
        [ -z $target ] && target="MUSDK.podspec"
        result=$(findMUSDKSpec $version $lang $build)
        ;;
      JB)
        [ -z $version ] && version="3.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="global" # global|jp
        result=$(findJB $version $lang $build $filter)
        ;;
      DP)
        [ -z $version ] && version="3.0"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="Global" # Global|JP
        result=$(findDP $version $lang $build $filter)
        ;;
      SS)
        [ -z $version ] && version="2.6"
        [ -z $lang ] && lang="en"
        [ -z $build ] && build="Latest"
        [ -z $filter ] && filter="SmartSurfing_Ent_Adhoc"
        result=$(findSS $version $lang $build $filter)
        ;;
      *)
        echo "Unsupported project."
        ;;
    esac
    currentBuild=$(echo "$result" | head -n1 | cut -f3 -d' ')
    srcPath=$(echo "$result" | tail -n1)
    echo "Fetching: $projectName $version $lang $currentBuild $filter"
    if [ -n "$target" ]; then
      fetch "$srcPath" "$prefix/$target"
      echo "$version.$currentBuild" > "$prefix/${target%.*}"
    else
      ext="${srcPath##*.}"
      fetch $srcPath "${prefix}/${projectName}_${version}_${lang}_${currentBuild}_${filter}.${ext}"
    fi

    ## Unmount
    #umount "$mountPoint"

fi
